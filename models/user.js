const mongoose = require('mongoose');
const validator = require("validator")
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Invalid Email..')
            }
        }
    },
    password: {
        type: String,
        trim: true,
        required: true,
        minlength: 7,
        validate(v) {
            if (v.toLowerCase().includes("password")) throw new Error("can't give 'password' in password")
        }
    },
    name: String,
});

module.exports = mongoose.model('user', userSchema, 'users');
const config = require("../config/config");
const jwt = require('jsonwebtoken')

exports.verifyUserToken = async (req, res, next) => {
    let token = req.header.authorization
    if (!token) return res.status(401).send("Access Denied / Unauthorized request");
    console.log("TOken ",token)
    try {
        token = token.split(' ')[1] // Remove Bearer from string

        if (token === 'null' || !token) return res.status(401).send('Unauthorized request');
        console.log("1")
        let verifiedUser = await jwt.verify(token, config.TOKEN_SECRET);   // config.TOKEN_SECRET => 'secretKey'
        if (!verifiedUser) return res.status(401).send('Unauthorized request')
        console.log("2")
        req.user = verifiedUser; // user_id & userRole & name
        next();

    } catch (error) {
        res.status(400).send("Invalid Token");
    }

}

const mongoose = require('mongoose');
const config = require("../config/config");
const jwt = require("jsonwebtoken");
const User = require("../models/user.js");

// Connect to DB
const db = config.DB_HOST;
mongoose.connect(db, function (err) {
    if (err) {
        console.error('Error! ' + err)
    } else {
        console.log('Connected to mongodb')
    }
});

exports.register = async (req, res) => {

    // Create an user object
    let user = new User({
        email: req.body.email,
        name: req.body.name,
        password: req.body.password,
    })
    try {
        await user.save()
        const token = jwt.sign({ id: user._id }, config.TOKEN_SECRET);
        res.status(200).send({ token })

    } catch (e) {
        res.status(400).send(e.message)
    }

}

exports.login = async (req, res) => {

    try {
        const user = await User.findOne({ email: req.body.email })

        if (user) {
            const validPass = req.body.password === user.password
            if (!validPass) return res.status(401).send("Invali credentials..");
            const token = jwt.sign({ id: user._id }, config.TOKEN_SECRET);
            req.header.authorization = "B " + token;
            res.status(200).header("auth-token", token).send({ "token": token });
        }
        else {
            res.status(401).send('Invali credentials..')
        }

    } catch (e) {
        res.status(500).send(e.message)
    }
}
exports.signout = async (req, res) => {
    try {
        req.header.authorization = null;
        return res.status(200).send({ message: " You've been signed out!" });
    } catch (err) {
        this.next(err);
    }
};

exports.userEvent = (req, res) => {
    res.json({ "message": "this events is only for Users" })
};


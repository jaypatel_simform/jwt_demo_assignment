const express = require('express');
const app = express();
app.use(express.json()); 
const auth = require('./routes/index');
 
app.use('/api', auth);

const port = process.env.port || 8000;
app.listen(port, function(){
    console.log("Server running on localhost:" + port);
})